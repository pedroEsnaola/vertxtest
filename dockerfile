FROM openjdk:8-jre-alpine
VOLUME /tmp


ENV _JAVA_OPTIONS "-Xms256m -Xmx1024m -Djava.awt.headless=true"

COPY target/test-0.0.1-SNAPSHOT.jar /opt/app.jar

RUN addgroup bootapp && \
    adduser -D -S -h /var/cache/bootapp -s /sbin/nologin -G bootapp bootapp

WORKDIR /opt
USER bootapp
ENTRYPOINT ["java", "-Djava.security.egd=file:/dev/./urandom", "-jar", "/opt/app.jar"]