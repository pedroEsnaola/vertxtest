package com.test.controller;

import com.test.model.AuthenticatedUser;
import com.test.model.User;
import com.test.repository.UserRepository;
import io.vertx.core.WorkerExecutor;
import io.vertx.ext.mongo.MongoClient;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

@Component
@Path("/")
@Slf4j
public class UserController {

    @Autowired
    MongoClient repo;

    @Autowired
    UserRepository repo1;

    @Autowired
    WorkerExecutor executor;

    @POST
    @Path("unprotected/user")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public User save(User user) {
        executor.<User>executeBlocking(future -> {

            // Do the blocking operation in here

            // Imagine this was a call to a blocking API to get the result
            User u = new User();
            try {
                u = repo1.save(user);
            } catch (Exception ignore) {
                log.error(ignore.getMessage());
            }

            future.complete(u);

        }, res -> {

            if (res.succeeded()) {

                log.info("Persisted user with id:" + res.result().getId());

            } else {
                res.cause().printStackTrace();
            }
        });
        return user;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("protected/user")
    public Page<User> findAll(@QueryParam("page") int page, @QueryParam("size") int size) {

        return repo1.findAll(PageRequest.of(page, size));

    }

    @GET
    @Path("/protected/me")
    @Produces(MediaType.APPLICATION_JSON)
    public AuthenticatedUser authenticated(@Context AuthenticatedUser user) {
        return user;
    }


}
