package com.test.util;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.extern.slf4j.Slf4j;

import java.util.Date;
import java.util.Map;

@Slf4j
public class JWTUtil {

    String secret = "secret";

    private String expirationTime = "259200";

    public String doGenerateToken(Map<String, Object> claims, String username) {
        Long expirationTimeLong = Long.parseLong(expirationTime); // em segundos

        final Date createdDate = new Date();
        final Date expirationDate = new Date(createdDate.getTime() + expirationTimeLong * 1000);
        try {
            return Jwts.builder().setClaims(claims).setSubject(username).setIssuedAt(createdDate)
                    .setExpiration(expirationDate).signWith(SignatureAlgorithm.HS256, secret).compact();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public Claims getAllClaimsFromToken(String token) {
        try {
            return Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody();
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return null;
    }


}
