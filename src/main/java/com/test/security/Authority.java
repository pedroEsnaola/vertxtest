package com.test.security;

import org.springframework.security.core.GrantedAuthority;

public enum Authority implements GrantedAuthority {
    ADMIN("ADMIN"),USER("USER");

    String auth;

    Authority(String auth){
        this.auth = auth;
    }

    @Override
    public String getAuthority() {
        return auth;
    }
}
