package com.test.security;


import com.test.model.AuthenticatedUser;
import com.zandero.rest.context.ContextProvider;
import io.jsonwebtoken.Claims;
import io.vertx.core.http.HttpServerRequest;
import org.springframework.stereotype.Component;

@Component
public class AuthenticationContextProvider implements ContextProvider<AuthenticatedUser> {


    AuthenticationProvider authenticationProvider = new AuthenticationProvider();

    @Override
    public AuthenticatedUser provide(HttpServerRequest httpServerRequest) throws Throwable {
        String token = httpServerRequest.getHeader("Authorization").replace("Bearer ", "");
        Claims c = authenticationProvider.getAllClaimsFromToken(token);
        AuthenticatedUser u = new AuthenticatedUser();
        u.setName(c.get("sub", String.class));
        u.setPassword(c.get("password", String.class));
        u.setId(c.get("id", String.class));
        u.setEmail(c.get("email", String.class));
        return u;
    }
}

