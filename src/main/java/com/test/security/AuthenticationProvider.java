package com.test.security;

import com.test.model.AuthenticatedUser;
import com.test.model.User;
import com.test.repository.UserRepository;
import com.test.util.JWTUtil;
import io.vertx.core.json.JsonObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Slf4j
@Service
public class AuthenticationProvider {



    JWTUtil jwtUtil = new JWTUtil();

    @Autowired
    UserRepository repo;



    public String generateToken(JsonObject auth) {
        User user = repo.findByEmail(auth.getString("email"));
        if (user != null) {
            Map<String, Object> claims = new HashMap<>();
            AuthenticatedUser data = AuthenticatedUser.fromUser(user);
            claims.put("id", data.getId());
            claims.put("email", data.getEmail());
            claims.put("password", data.getPassword());
            return jwtUtil.doGenerateToken(claims, data.getName());
        } else {
            return null;
        }

    }




}
