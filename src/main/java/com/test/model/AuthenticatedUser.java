package com.test.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.data.annotation.Id;

import java.io.Serializable;

public class AuthenticatedUser implements Serializable {


    @Id
    private String id;

    private String name;

    @JsonIgnore
    private String password;

    private String email;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public AuthenticatedUser(String id, String name, String password, String email) {
        this.id = id;
        this.name = name;
        this.password = password;
        this.email = email;
    }

    public AuthenticatedUser() {

    }

    public static AuthenticatedUser fromUser(User user) {
        AuthenticatedUser u = new AuthenticatedUser();
        u.id = user.getId();
        u.name = user.getName();
        u.password = user.getPassword();
        u.email = user.getEmail();
        return u;
    }

}
