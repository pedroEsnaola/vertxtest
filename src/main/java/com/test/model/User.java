package com.test.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.test.security.Authority;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.List;

@Document(collection = "users")
public class User {

    @Id
    private String id;

    private String name;

    private List<String> authorities;

    @JsonIgnore
    private String password;

    private String email;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAuthorities(List<String> authorities) {
        this.authorities = authorities;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<String> getAuthorities() {
        return authorities;
    }

    public String getPassword() {
        return password;
    }

    public User(String id, String name, List<String> authorities, String password, String email) {
        this.id = id;
        this.name = name;
        this.authorities = authorities;
        this.password = password;
        this.email = email;
    }

    public User() {

    }

}
