package com.test.model;

public class AuthenticationResponse {

    String token;

    int duration;

    void setToken(String token){
        this.token = token;
    }

    String getToken(){
        return token;
    }

    public int getDuration() {        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public AuthenticationResponse(String token, int duration) {
        this.token = token;
        this.duration = duration;
    }

    public AuthenticationResponse(){

    }
}
