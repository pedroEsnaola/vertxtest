package com.test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.test.controller.UserController;
import com.test.model.AuthenticatedUser;
import com.test.security.AuthenticationContextProvider;
import com.test.security.AuthenticationProvider;
import com.zandero.rest.RestRouter;
import io.vertx.core.*;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.mongo.MongoClient;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.micrometer.MicrometerMetricsOptions;
import io.vertx.micrometer.PrometheusScrapingHandler;
import io.vertx.micrometer.VertxPrometheusOptions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;

import javax.annotation.PostConstruct;

@SpringBootApplication
public class TestApplication {

    public static void main(String[] args) {
        SpringApplication.run(TestApplication.class, args);
    }


    @Autowired
    UserController controller;


    ObjectMapper mapper = new ObjectMapper();

    Vertx vertx = Vertx.vertx(new VertxOptions().setMetricsOptions(
            new MicrometerMetricsOptions()
                    .setPrometheusOptions(new VertxPrometheusOptions().setEnabled(true))
                    .setEnabled(true)));


    @Autowired
    AuthenticationProvider provider;


    @PostConstruct
    void init() {

        Router router = RestRouter.register(vertx, controller);
        router.route("/metrics").handler(PrometheusScrapingHandler.create());
        RestRouter.addProvider(AuthenticatedUser.class, AuthenticationContextProvider.class);

        router.route("/protected/*").handler(ctx -> {
            if (ctx.request().getHeader("Authorization") != null) ctx.next();
            else ctx.fail(401);

        });
        router.route("/unprotected/newToken").handler(BodyHandler.create());

        router.get("/unprotected/newToken").handler(ctx -> {

            JsonObject auth = ctx.getBodyAsJson();
            String token = provider.generateToken(auth);
            if (token != null) {
                ctx.response().setStatusCode(200).end(token);
            } else
                ctx.fail(401);
        });


        vertx.deployVerticle(() -> new AbstractVerticle() {
            @Override
            public void start(Future<Void> startFuture) {
                vertx.createHttpServer()
                        .requestHandler(router)
                        .listen(8081);
            }
        }, new DeploymentOptions().setInstances(2));
    }

    @Bean
    MongoClient mongoClient() {
        return MongoClient.createShared(vertx, new JsonObject());
    }

    @Bean
    @Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
    WorkerExecutor workerExecutor() {
        return vertx.createSharedWorkerExecutor("ServiceExecutor", 20);
    }


}
