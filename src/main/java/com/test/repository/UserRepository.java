package com.test.repository;

import com.test.model.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;


@Service
public interface UserRepository extends MongoRepository<User, String> {
    User findByEmail(String email);
}
